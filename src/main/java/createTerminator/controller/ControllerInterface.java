package createTerminator.controller;

public interface ControllerInterface {

    void setTerminatorName(String terminatorName) ;
    void setTerminatorModel(String terminatorModel);

    void setTerminatorSpeedLevel(int terminatorSpeedLevel);
    void setTerminatorStrengthLevel(int terminatorStrengthLevel);
    void setPowerLevel();
    String getCompleteTerminator();
}
