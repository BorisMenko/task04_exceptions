package createTerminator.model;

public class TerminatorBody {
    private String name;
    private String model;
    private int speedLevel;
    private int strengthLevel;
    private int powerLevel;

    public TerminatorBody() {
    }

    public TerminatorBody(String name, String model, int speedLevel, int strengthLevel) {
        this.name = name;
        this.model = model;
        this.speedLevel = speedLevel;
        this.strengthLevel = strengthLevel;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPowerLevel() {
        return powerLevel;
    }

    public void setPowerLevel(int strengthLevel, int speedLevel) {
        try {
            this.powerLevel = speedLevel * strengthLevel;
        } catch (ArithmeticException e) {
            System.out.println("You enter wrong parameters");
        }
    }

    public int getSpeedLevel() {
        return speedLevel;
    }

    public void setSpeedLevel(int speedLevel) {
        this.speedLevel = speedLevel;
    }

    public int getStrengthLevel() {
        return strengthLevel;
    }

    public void setStrengthLevel(int strengthLevel) {
        this.strengthLevel = strengthLevel;
    }

    @Override
    public String toString() {
        return "TerminatorBody{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", speedLevel=" + speedLevel +
                ", strengthLevel=" + strengthLevel +
                ", powerLevel=" + powerLevel +
                '}';
    }
}
