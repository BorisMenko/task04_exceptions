package createTerminator.controller;

import createTerminator.model.TerminatorBody;


public class Controller implements ControllerInterface {
    TerminatorBody terminator = new TerminatorBody();

    @Override
    public void setTerminatorName(String terminatorName) {
            terminator.setName(terminatorName);
    }

    @Override
    public void setTerminatorModel(String terminatorModel) {
        terminator.setModel(terminatorModel);
    }

    @Override
    public void setTerminatorSpeedLevel(int terminatorSpeedLevel) {
        terminator.setSpeedLevel(terminatorSpeedLevel);

    }

    @Override
    public void setTerminatorStrengthLevel(int terminatorStrengthLevel) {
        terminator.setStrengthLevel(terminatorStrengthLevel);
    }

    @Override
    public void setPowerLevel() {
        terminator.setPowerLevel(terminator.getSpeedLevel(), terminator.getPowerLevel());
    }

    @Override
    public String getCompleteTerminator() {

        System.out.println(terminator.toString());
        return terminator.toString();
    }


}
