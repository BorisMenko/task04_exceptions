package createTerminator.view;

import createTerminator.controller.Controller;
import createTerminator.controller.ControllerInterface;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class View implements Printable, AutoCloseable {

    private ControllerInterface controller = new Controller();
    private static Scanner input = new Scanner(System.in);

    public void generalMethod() throws IOException {
        View view = new View();
        view.terminatorName();
        view.terminatorModel();
        view.terminatorSpeedAndStrengthLevel();
        view.print();
        view.writeTerminatorInformationIntoFile();
    }

    public void terminatorName() {
        System.out.println("Enter terminator name");
        try {
            String terminatorName = input.nextLine();
            System.out.println("You set terminator name " + " <" + terminatorName + ">");
            controller.setTerminatorName(terminatorName);
        } catch (NullPointerException | NumberFormatException e) {
            System.out.println("You enter wrong name");
        }


    }

    public void terminatorModel() {
        System.out.println("Select terminator model");
        ArrayList<String> terminatorModel = new ArrayList<>();
        try {
            terminatorModel.add("Press 1 for select T-800");
            terminatorModel.add("Press 2 for select T-1000");
            terminatorModel.add("Press 3 for select RV-9");
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println("Please select right model");
        }
        for (String s : terminatorModel) {
            System.out.println(s);
        }
        int selectNumber = input.nextInt();
        if (selectNumber == 1) {
            controller.setTerminatorModel(terminatorModel.get(0));
        } else if (selectNumber == 2) {
            controller.setTerminatorModel(terminatorModel.get(1));
        } else controller.setTerminatorModel(terminatorModel.get(2));
    }

    public void terminatorSpeedAndStrengthLevel() throws IOException {
        System.out.println("Select terminator speed level from 1 to 5");
        int speedLevel = input.nextInt();
        System.out.println("Now select terminator strength from 1 to 5");
        System.out.println("But if you selected high speed you can't select high strength");
        int strengthLevel = input.nextInt();
        if (speedLevel == 5 && strengthLevel == 5) {
            System.out.println("Wrong select, try again");
            speedLevel = input.nextInt();
            strengthLevel = input.nextInt();
        }

        controller.setTerminatorSpeedLevel(speedLevel);
        controller.setTerminatorStrengthLevel(strengthLevel);
    }


    @Override
    public void print() {
        controller.getCompleteTerminator();

    }

    private void writeTerminatorInformationIntoFile() {
        System.out.println("Save information about terminator is starting.");
        String allInfo = controller.getCompleteTerminator();
        String fileName = "Terminators.txt";
        FileOutputStream outFile;
        try {
            outFile = new FileOutputStream(fileName);
            System.out.println("Output file was opened.");
            outFile.write(allInfo.getBytes());
            System.out.println("Saved: " + allInfo);
            outFile.close();
            System.out.println("Output stream was closed.");
        } catch (IOException e) {
            System.out.println("File Write Error: " + fileName);
        }
    }


    @Override
    public void close() throws Exception {
        System.out.println("Closing!");

    }
}
